<?php
/**
A handler for all kinds of requests coming to the application.
Also is in charge of forwarding to the correct controller.
*/
class FrontController {

    function __construct() {
        $url = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        if (sizeof($url) == 1) {
            header("Location: http://www.followtasks.com/index#log");
        }
        elseif ($url[1] != "index" && !isset($_SESSION['user_type']) && !isset($_SESSION['user_id'])) {
            
        }
        $controllerName = ucfirst($url[1]) . 'Controller';
        $file = 'controllers/' . ucfirst($url[1]) . 'Controller.php';
        if (!file_exists($file)) {
            $controller = new ErrorController();
            $controller->index();
        } else {
            $controller = new $controllerName();
            if (isset($url[4])) {
                $controller->{$url[2]}($url[3], $url[4]);
            } else if (isset($url[3])) {
                $controller->{$url[2]}($url[3]);
            } else {
                if (isset($url[2])) {
                    if (method_exists($controller, $url[2])) {
                        $controller->{$url[2]}();
                    }
                } else {
                    $controller->index();
                }
            }
        }
    }

}
