<?php
/**
All things associated with the database are done with the help of this class.
*/
abstract class Model {

    public $db;
    /**
    Access to the database.
    */
    public function __construct() {
        $this->db = new PDO('mysql:host=wm74.wedos.net;dbname=d85771_tasks','a85771_tasks','tinheslo');
    }
    /**
    Make a query to the database.
    */
    /** @param string $query */
    /** @param string[] $params Parameters. */
    public function doQuery($query, $params = array()){
        $stm = $this->db->prepare($query);
        $stm->execute($params);
        if (!$stm) {
            $error = $this->db->errorInfo();
            echo $error[2]; 
            return NULL;
        }
        else {
            return $stm;
        }
    }

}