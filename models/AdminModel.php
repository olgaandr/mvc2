<?php
/** @package Models */
class AdminModel extends Model{
    
    /**
    Adds new class.
     * @param integer $class_name Student id. 
     */
    function addClass($class_name){
        $query = "INSERT INTO `d85771_tasks`.`class` (`id`, `name`) VALUES (NULL, :id);";
        $this->doQuery($query, array(':id' => $class_name));
    }
    
    /**
    Adds achievement.
     * @param Achievement $achievement 
     */
    function addAchievement(Achievement $achievement){
        $query = "INSERT INTO `d85771_tasks`.`achievement` (`id`, `text`, `type`, `value`, `reward`)
                  VALUES (NULL, :text, :type, :value, :reward);";
        $this->doQuery($query, array(':text' => $achievement->text,':type' => $achievement->type,
                ':value' => $achievement->value,':reward' => $achievement->reward));
    }
    
    /**
    Adds new type of achievement.
     * @param string $type Type name. 
     */
    function addAchievementsType($type){
        $query = "INSERT INTO `d85771_tasks`.`achievement_type` (`id`, `type`) VALUES (NULL, :type)";
        $this->doQuery($query, array(':type' => $type));
    }
    
    
}
