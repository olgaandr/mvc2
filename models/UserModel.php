<?php
/** @package Models */
class UserModel extends Model {

    /**
    Returns user if exists in database.
     * @param string $email
     * @param string $password 
     * @return User 
     */
    function getUserIfExist($email, $password) {
        require_once 'models/databaseObjects/User.php';
        $email = htmlspecialchars($email);
        $password = htmlspecialchars($password);
        $student = $this->getUser($email, $password,1);
        if ($student == NULL) {
            return $this->getUser($email, $password,2);
        } else {
            return $student;
        }
    }
    
    /**
    Returns user if exists in database
     * @param string $email
     * @param string $password 
     * @param integer $type Student or teacher. 
     * @return User 
     */
    private function getUser($email, $password, $type) {
        if($type==1){
            $string = "student";
        }
        else if($type==2){
            $string = "teacher";
        }
        $query = 'SELECT id, email, password FROM '.$string.' WHERE email = :mail AND password = :pass';
        
        $stm = $this->doQuery($query, array(':mail' => $email,':pass'=>$password));
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        if($row == NULL){
            return NULL;
        }else {
            $user = new User($row["id"], $email, $password, $type);
            return $user;
        }
    }
    
    /**
    Returns user by id.
     * @param integer $user_id
     * @param integer $type 
     * @return User
     */
    function getUserById($user_id,$type) {
        if($type==1){
            $string = "student";
        } else{
            $string = "teacher";
        }
        require_once 'models/databaseObjects/'.ucfirst($string).'.php';
        $query = "SELECT * FROM ".$string." WHERE id=:id";
        
        $stm = $this->doQuery($query,array(':id' => $user_id));
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        if($row == NULL){
            return NULL;
        }else {
            if($type==1){
                $homeworksModel = new HomeworksModel();
                $hwComCount = $homeworksModel->getCountOfHomeworks($user_id, true);
                $hwIncomCount = $homeworksModel->getCountOfHomeworks($user_id, false);
                $level = $this->checkStudentLevel($user_id, intval($row["level"]), 
                        intval($row["homeworks_to_next_level"]), intval($hwComCount));
                    
                $user = new Student($row["id"], $row["name"], $row["email"], $row["password"], 
                        $row["coins"], $level, $row["homeworks_to_next_level"],  
                        $hwComCount, $hwIncomCount, $row["class_id"], 1);
                
            } else{
                $improvementsModel = new ImprovementsModel();
                $numImp = $improvementsModel->getNumberOfTeacherImprovements($user_id);
                $homeworksModel = new HomeworksModel();
                $hwMarkCount = $homeworksModel->getHomeworksWaitingForMark($user_id);
                $hwCount = $homeworksModel->getTeacherAddedHomeworks($user_id);
                $user = new Teacher($row["id"], $row["name"], $row["email"], $row["password"],
                        $hwCount,$numImp,$hwMarkCount, 2);
            }
            return $user;
        }
    }
    
    /**
    Returns user by email.
     * @param string $email 
     * @return User 
     */
    function getUserByEmail($email){
        require_once 'models/databaseObjects/User.php';
        
        $query = 'SELECT id, email, password FROM student WHERE email = :mail';
        $query2 = 'SELECT id, email, password FROM teacher WHERE email = :mail';
        
        $stm = $this->doQuery($query, array(':mail' => $email));
        $stm2 = $this->doQuery($query2, array(':mail' => $email));
        
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        $row2 = $stm2->fetch(PDO::FETCH_ASSOC);
        
        if($row == NULL){
            if($row2 == NULL){
                return NULL;
            } else{
                $user = new User($row["id"], $email, $row["password"], 2);
                return $user;
            }
        }else {
            $user = new User($row["id"], $email, $row["password"], 1);
            return $user;
        }
    }
    
    /**
    Changes user password
     * @param integer $user_id
     * @param string $newPassword
     * @param integer $type 
     */
    function changePassword($user_id, $newPassword, $type = 1) {
        if($type==1){
            $string = "student";
        }
        else if($type==2){
            $string = "teacher";
        }
        $query = "UPDATE `$string` SET `password` = :pass WHERE `student`.`id` = :id;";
        $stm = $this->doQuery($query, array(':pass' => $newPassword,':id'=> intval($user_id)));
        return $stm;
    }
    
    /**
    Checks if an email is already in database.
     * @param string $email 
     * @param integer $type 
     * @return boolean
     */
    function isMailUnique($email, $type) {
        if($type==1){
            $string = "student";
        }
        else if($type==2){
            $string = "teacher";
        }
        $query = "SELECT email FROM $string WHERE email = :email";
        $stm = $this->doQuery($query, array(':email' => $email));
        if($stm->rowCount() == 0){
            return true;
        }else {
            return false;
        }
    }
    
    /**
    Register user
     * @param string $name
     * @param string $email
     * @param string $hashpass Hashed password
     * @param integer $class 
     */
    function registerUser($name, $email, $hashpass, $class) {
        $class = intval($class);
        if ($class == 0) {
            $query = 'INSERT INTO `teacher` (`id`,`name`,`email`,`password`)
              VALUES (NULL, :name, :email, :password)';
            $this->doQuery($query, array(':name' => $name, ':email' => $email,
                ':password' => $hashpass));
        } else {
            $query = "INSERT INTO `student` 
            (`id`,`name`,`email`,`password`,`coins`,`level`,`homeworks_to_next_level`,`class_id`)
            VALUES (NULL,:name,:email,:password,'0','1','5',:class)";
            $this->doQuery($query, array(':name' => $name, ':email' => $email,
                ':password' => $hashpass, ':class' => $class));
        }
    }
    
    /**
    Returns student by class.
     * @param integer $class_id 
     * @return SimpleStudent[]
     */
    function getStudentsByClass($class_id){
        require_once 'models/databaseObjects/SimpleStudent.php';
        $query = "SELECT s.id, s.name FROM `student` AS s WHERE class_id = :class_id";
        $stm = $this->doQuery($query, array(':class_id' => $class_id));
        while($row = $stm->fetch(PDO::FETCH_ASSOC)){
            $students[] = new SimpleStudent($row['id'],$row['name']);
        }
        return $students;
    }
    
    /**
     * @param integer $student_id 
     * @param integer $level 
     * @param integer $hwToNextLvl  Tasks to next level
     * @param integer $comHw Number of completed tasks  
     */
    function checkStudentLevel($student_id, $level, $hwToNextLvl, $comHw){
        if($hwToNextLvl == $comHw){
            $newLvl = $level + 1;
            if($level < 10){
                $hwToNextLvl = $hwToNextLvl + 5;
            } else{
                $hwToNextLvl = $hwToNextLvl + ($level - 5);
            }
            
            $query = "UPDATE `student` SET `level` = :lvl, homeworks_to_next_level = :next WHERE `student`.`id` = :id;";
            $this->doQuery($query, array(':lvl' => $newLvl, ':next' => $hwToNextLvl, ':id' => $student_id));
        
            return $newLvl;
        }else{
            return $level;
        }
    }
}