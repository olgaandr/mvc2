<?php
/** @package Models */
class AchievementsModel extends Model{
    /**
    Selects all student´s achievements.
     * @param integer $user_id Student id.
     * @return Achievement[] Array of achievements. 
     */
    function getUserAchievements($user_id){
        require_once 'models/databaseObjects/Achievement.php';
        $query = 'SELECT a.id, `text`, value, at.type, reward, CASE WHEN sha.student_id=:id THEN "complete" ELSE "incomplete" END AS complete
                  FROM `achievement` AS a LEFT JOIN student_has_achievement AS sha ON(sha.achievement_id = a.id) 
                  JOIN achievement_type AS at ON(a.type = at.id)
                  WHERE a.type != 4 OR (a.type = 4 AND sha.student_id = :id) ORDER BY complete';
        $stm = $this->doQuery($query, array(':id' => $user_id));
        $achievementsArray = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)){
            $achievementsArray[] = new Achievement($row["id"], $row["text"], $row["type"], 
                    $row["value"], $row["reward"],$row["complete"]);
            
            
        }
        return $achievementsArray;
    }
    
    /**
    Selects all achievements added by teacher.
     * @param integer $teacher_id Teacher id.
     * @return TeacherAchievement[] Array of achievements. 
     */
    function getTeacherAchievements($teacher_id){
        require_once 'models/databaseObjects/TeacherAchievement.php';
        $query = "SELECT a.id, a.text, a.reward, COUNT( achievement_id ) AS count, s.name
                  FROM  `student_has_achievement` JOIN achievement AS a ON(a.id = `achievement_id`)
                  JOIN student AS s ON(s.id = student_id) WHERE teacher_id =3 GROUP BY achievement_id";
        $stm = $this->doQuery($query, array(':id' => $teacher_id));
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)){
            $achievementsArray[] = new TeacherAchievement($row['id'],$row['text'],$row['reward'],$row['count'],$row['name']);
        }
        return $achievementsArray;
    }
    
    /**
    Adds new achievement to student.
     * @param Student $user
     * @param Achievement $achievement
     * @param integer $teacher_id (optional) 
     */
    function addAchievement(Student $user, Achievement $achievement, $teacher_id = "NULL"){
        $query = "INSERT INTO `d85771_tasks`.`achievement` "
                . "(`id`, `text`, `type`, `value`, `reward`) "
                . "VALUES (NULL, :text, '4', :value, :reward);";
        $this->doQuery($query, array(':text' => $achievement->text,
            ':value' => $achievement->value, ':reward' => $achievement->reward));
        
        $lastInsertId = $this->db->lastInsertId();
        $this->addStudentAchievement($user, $achievement, $lastInsertId, $teacher_id);
    }
    
    private function addStudentAchievement(Student $user, Achievement $achievement, $lastInsertId, $teacher_id = NULL){
        $query2 = "INSERT INTO `d85771_tasks`.`student_has_achievement` 
                 (`student_id`, `achievement_id`, `teacher_id`) VALUES (:student_id, :achievement_id, :teacher_id);";
        $this->doQuery($query2, array(':student_id' => $user->id,':achievement_id' => $lastInsertId,
            ":teacher_id" => $teacher_id));
        
        $coins = $user->coins + $achievement->reward;
        $query3 = "UPDATE `student` SET `coins` = :coins WHERE `student`.`id` =:id;";
        $this->doQuery($query3, array(':coins' => $coins,':id' => $user->id));
    }


    /**
    Checks if student has new achievement.
     * @param Student $user 
     */
    function checkStudentAchievments(Student $user){
        require_once 'models/databaseObjects/Achievement.php';
        $userAchievements = $this->getUserAchievements($user->id);
        foreach($userAchievements as $achievement){
            
            if ($achievement->complete == "incomplete") {
                $this->addNewIfExists($user, $achievement);
            }
        }
    }
    
    /**
    Adds new achievement to student if student complied with the conditions.
     * @param Student $user 
     */
    private function addNewIfExists(Student $user, Achievement $achievement) {
        if ($achievement->type == "homeworks" && $achievement->value <= $user->hwComCount) {
            $this->addStudentAchievement($user,$achievement,$achievement->id);
        }
        if ($achievement->type == "levels" && $achievement->value <= $user->level) {
            $this->addStudentAchievement($user,$achievement,$achievement->id);
        }
        if ($achievement->type == "coins" && $achievement->value <= $user->coins) {
            $this->addStudentAchievement($user,$achievement,$achievement->id);
        }
    }
    
    /**
    Returns types of achievements.
     * @return AchievementType[] Array of types. 
     */
    function getTypes(){
        require_once 'models/databaseObjects/AchievementType.php';
        $query = "SELECT id,type FROM `achievement_type`";
        $stm = $this->doQuery($query);
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)){
            $typeArray[] = new AchievementType($row["id"],$row["type"]);
        }
        return $typeArray;
    }
    
    /**
    Deletes an achievement.
     * @param integer $id Achievement id. 
     */
    function deleteAchievement($id){
        $query = "DELETE FROM `d85771_tasks`.`student_has_achievement` WHERE achievement_id = :id";
        $this->doQuery($query,array(":id"=>$id));
        $query2 = "DELETE FROM `d85771_tasks`.`achievement` WHERE achievement.id = :id";
        $this->doQuery($query2,array(":id"=>$id));
    }
}
