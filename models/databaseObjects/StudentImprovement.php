<?php
require_once 'Improvement.php';

class StudentImprovement extends Improvement{
    public $subject_name;
    public $subject_id;
    public $obtained;
    
    function __construct($id,$text,$price,$subject_name,$obtained,$subject_id = 0) {
        parent::__construct($id, $text, $price,$subject_name,$subject_id);
        //$this->subject_name = $subject_name;
        //$this->subject_id = $subject_id;
        $this->obtained = $obtained;
    }
}

