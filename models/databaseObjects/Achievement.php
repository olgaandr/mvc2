<?php
class Achievement {
    public $id;
    public $text;
    public $reward;
    public $complete;
    public $type;
    public $value;
    
    public function __construct($id,$text,$type,$value,$reward,$complete) {
        $this->id = $id;
        $this->text = $text;
        $this->type = $type;
        $this->value = $value;
        $this->reward = $reward;
        $this->complete = $complete;
    }
}
