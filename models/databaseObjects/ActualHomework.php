<?php
require_once 'Homework.php';

class ActualHomework extends Homework{
    public $date;
    public $subject;
            
    function __construct($id, $name, $text, $reward, $mark, $status, $date, $subject) {
        parent::__construct($id, $name, $text, $reward, $mark, $status);
        $this->date = $date;
        $this->subject = $subject;
    }
}
