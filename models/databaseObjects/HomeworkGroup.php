<?php
require_once 'Homework.php';

class HomeworkGroup extends Homework{
    public $needMark;
    
    function __construct($id, $name, $text, $reward, $mark, $needMark, $status = 0) {
        parent::__construct($id, $name, $text, $reward, $mark, $status);
        $this->needMark = $needMark;
        
    }
}
