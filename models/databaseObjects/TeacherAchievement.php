<?php

class TeacherAchievement {
    public $id;
    public $text;
    public $reward;
    public $count;
    public $studentName;


    public function __construct($id,$text, $reward, $count, $studentName) {
        $this->text = $text;
        $this->id = $id;
        $this->count = $count;
        $this->reward = $reward;
        $this->studentName = $studentName;
    }
}
