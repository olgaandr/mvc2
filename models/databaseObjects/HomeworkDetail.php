<?php
require_once 'Homework.php';

class HomeworkDetail extends Homework{
    public $student_id;
    public $student_name;
    
    public function __construct($id, $name, $text, $reward, $mark, $student_id, $student_name, $status = 0) {
        parent::__construct($id, $name, $text, $reward, $mark, $status);
        $this->student_id = $student_id;
        $this->student_name = $student_name;
    }
}
