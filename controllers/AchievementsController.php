<?php
/** @package Controllers */
class AchievementsController extends Controller {
    /**
    If user is student it shows all achievements. If user is teacher it shows
    achievements created by this teacher and also form for adding a new achievement
    to one or more students.
     * @uses $_SESSION['user_id']
     * @uses $_SESSION['user_type'] 
     */
    function index() {
        $userModel = new UserModel();
        $user = $userModel->getUserById($_SESSION["user_id"], $_SESSION["user_type"]);
        $achievementsModel = new AchievementsModel();
        
        if ($_SESSION['user_type'] == 2) {
            $homeworksModel = new HomeworksModel();
            $subjects = $homeworksModel->getTeacherSubjects($user->id);
            $classes = $homeworksModel->getClasses();
            $students = $userModel->getStudentsByClass($classes[2]->id);
            $achievements = $achievementsModel->getTeacherAchievements($user->id);
            echo $this->view->renderTwig('teacher_achievements.twig', array("achievements" => $achievements, 
                "subjects" => $subjects, "classes" => $classes, "students" => $students));
        } else {
            $achievements = $achievementsModel->getUserAchievements($user->id);
            $achievementsModel->checkStudentAchievments($user);
            
            echo $this->view->renderTwig('achievements.twig', array("achievements" => $achievements));
        }
    }
    
    /** This function is used by ajax to change students by choosen class. 
     * @param integer $class_id 
     */
    function changeClass($class_id){
        $userModel = new UserModel();
        $students = $userModel->getStudentsByClass($class_id);
        echo $this->view->renderTwig('teacher_achievements_students.twig', array("students" => $students));
    }
            
    /** Adds new achievement. Available just when logged user is teacher.
      * @uses $_SESSION['user_id']
      * @uses $_POST['text'] 
      * @uses $_POST['reward'] 
      * @uses $_POST['students'] Array of students ids. 
      * @uses $_POST["class"]
      */
    function addAchievement(){
        require_once 'models/databaseObjects/Achievement.php';
        $achievementsModel = new AchievementsModel();
        $students = $_POST["students"];
        
        $hm = new HomeworksModel();
        $um = new UserModel();
        $checkClasses = $hm->getClasses();
        $checkStudents = $um->getStudentsByClass($_POST["class"]);
        $checkPosts = $this->checkTextValue($_POST["text"]) && $this->checkPostNumberValue($_POST["reward"])
                && $this->checkPostedIdInArray($_POST['class'], $checkClasses);
        //echo "posts: ". $this->checkPostNumberValue($_POST["reward"]);
        
        if(!$checkPosts){
            $this->addMessage("achievements","Try again");
            $this->redirect("achievements");
        }
        else{
            $achievement = new Achievement(0, $_POST['text'], 4, 0, $_POST['reward'],"");
            foreach($students as $s){
                $userModel = new UserModel();
                if(!$this->checkPostedIdInArray($s, $checkStudents)){
                    $this->addMessage("achievements","Try again");
                    $this->redirect("achievements");
                } else{
                    $user = $userModel->getUserById($s, 1);
                    $achievementsModel->addAchievement($user, $achievement, $_SESSION['user_id']);
                }
            }
            $this->addMessage("achievements","Achievement added");
            $this->redirect("achievements");
        }
    }
    
    /** Deletes achievement. Only for teachers. 
     * @param integer $id Achievement id. 
     */
    function deleteAchievement($id){
        $achievementModel = new AchievementsModel();
        $achievementModel->deleteAchievement($id);
    }
}
