<?php
/**
  This class is for functioning of the mobile application.
 */
/** @package Controllers */
class PhoneController {

    /** @uses $_POST['user_id'] */
    function getUser() {
        $userModel = new UserModel();
        $user_id = $_POST["user_id"];
        $user = $userModel->getUserById($user_id, 1);
        header('Content-Type: application/json');
        echo json_encode($user);
    } 

    /** @uses $_POST['user_id'] */
    function getSubjects() {
        $userModel = new UserModel();
        $user = $userModel->getUserById($_POST["user_id"], 1);
        $hm = new HomeworksModel();
        $subjects = $hm->getSubjects($user->class_id);
        header('Content-Type: application/json');
        echo json_encode($subjects);
    }

    function getSectionsWithHomeworks() {
        $hm = new HomeworksModel();
        $sections = $hm->getSections($id);
        header('Content-Type: application/json');
        echo json_encode($sections);
    }

    /** @uses $_POST['user_id'] */
    function getActualHomeworks() {
        $userModel = new UserModel();
        $user = $userModel->getUserById($_POST["user_id"], 1);
        $hm = new HomeworksModel();
        $homeworks = $hm->getActualHomeworks($user->getId(), false);
        header('Content-Type: application/json');
        echo json_encode($homeworks);
    }

    /** @uses $_POST['user_id'] */
    function getPastHomeworks() {
        $userModel = new UserModel();
        $user = $userModel->getUserById($_POST["user_id"], 1);
        $hm = new HomeworksModel();
        $pastHomeworks = $hm->getActualHomeworks($user->getId(), true);
        header('Content-Type: application/json');
        echo json_encode($pastHomeworks);
    }

    function getAchievements() {
        $userModel = new UserModel();
        $user_id = 1;
        $user = $userModel->getUserById($user_id, 1);
        $achievementsModel = new AchievementsModel();
        $achievements = $achievementsModel->getUserAchievements($user->id);
        header('Content-Type: application/json');
        echo json_encode($achievements);
    }

}
