<?php
/** @package Controllers */
class AdminController extends Controller{
    /**
    Available just for admin. Shows forms for management.
    */
    function index(){
        $achievementsModel = new AchievementsModel();
        $types = $achievementsModel->getTypes();
        echo $this->view->renderTwig('admin.twig',array("types" => $types));
    }
    
    /**
    Checks if user who came to the page is admin. If not redirects to the index page.
     * @uses $_SESSION['user_id'] 
     */
    protected function unloggedUser(){
        if(!isset($_SESSION['user_id']) || (isset($_SESSION['user_id']) && $_SESSION['user_id'] != 0)) {
            $this->redirect("index", "#log");
        }
    }
    
    /**
    Adds new class.
     * @uses $_POST["class"] 
     */
    function addClass(){
        $adminModel = new AdminModel();
        $adminModel->addClass($_POST["class"]);
        $this->addMessage("adminClass", "Class added.");
        $this->redirect("admin");
    }
    
    /**
    Adds new achievement.
     * @uses $_POST["text"] 
     * @uses $_POST["type"] 
     * @uses $_POST["value"] 
     * @uses $_POST["reward"] 
     */
    function addAchievement(){
        require_once 'models/databaseObjects/Achievement.php';
        $achievement = new Achievement(0,$_POST["text"],$_POST["type"],$_POST["value"],$_POST["reward"],"incomplete");
        $adminModel = new AdminModel();
        $adminModel->addAchievement($achievement);
        $this->addMessage("adminAchievement", "Achievement added.");
        $this->redirect("admin");
    }
    
    /**
    Adds new achievement type.
     * @uses $_POST["type"]
     */
    function addAchievementsType(){
        $adminModel = new AdminModel();
        $adminModel->addAchievementsType($_POST["type"]);
        $this->addMessage("adminType", "Achievement type added.");
        $this->redirect("admin");
    }
    
    /**
    Logout admin.
     * @uses $_SESSION["user_id"] 
     */
    function logout() {
        $_SESSION = array();
        session_destroy();
        if (isset($_SESSION["user_id"])) {
            echo "ERROR: Cannot terminate session!";
        } else {
            $this->redirect("index", "#log");
        }
    }
}
