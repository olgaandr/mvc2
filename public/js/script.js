$(document).ready(function() {
    var path = window.location.hash;
    var p = window.location;
    if (path == "#log") {
        logClick();
    }
    else if (path == "#reg") {
        regClick();
    }
    //else if (p=="http://localhost/Mvc2/index") {
    else if (p=="http://followtasks.com/") {
        logClick();
    }
    
    //$('#classStudents').multiselect({includeSelectAllOption: true});
    //$('#classStudents').multiselect();
    
    /*$("#addImprovement").click(function() {
        var text = $("#impText").val();
        var subject = $("#userType option:selected").val();
        var price = $("#impPrice").val();
        //alert("text: "+text+", subject: "+subject+", price: "+price);
        
        $.ajax({
            type: 'POST',
            data: { text: text, subject: subject, price : price} ,
            url: 'improvements/addImprovement'
        });
    });*/
    $("#forgot").click(function(e) {
        e.preventDefault();
        $("#login").hide("slow");
        $("#forgotten").show("slow");
        //window.location.replace(window.location.pathname + '#forgotten');
    });
    $("#remember").click(function(e) {
        e.preventDefault();
        $("#forgotten").hide("slow");
        $("#login").show("slow");
        //window.location.replace(window.location.pathname + '#log');
    });
    
    $(".impDelete").click(function(e) {
        e.preventDefault();
        var id = $(this).attr("id");
        //var id = $(this).parent().attr("class").split(' ')[1];
        //alert(id);
        $.ajax({
            type: 'POST',
            data: { id: id} ,
            url: 'improvements/removeImprovement',
            success: function(){
                $("."+id).hide("slow");
                //alert(e); 
                //location.reload();
            }
        });
    });
    
    $(".achDelete").click(function(e) {
        e.preventDefault();
        //var id = $(this).attr("id");
        var id = $(this).parent().attr("class").split(' ')[1];
        //alert(id);
        $.ajax({
            type: 'POST',
            //data: { id: id} ,
            url: 'achievements/deleteAchievement/'+id,
            success: function(){
                $("."+id).hide("slow");
                //alert(e); 
                //location.reload();
            }
        });
    });
    
    $(".impRewrite").click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent().attr("class");
        var id = parent.split(' ')[1];
        var subid = $(this).parent().attr("class").split(' ')[1];
        //alert($("form").attr("action"));
        $('body').animate({ scrollTop: 0 }, 'slow', function(){
            var text = $("."+id+" .impText").text();
            var price = parseInt($("."+id+" .impPrice").text().split(' ')[1]);
            $("#impText").val(text);
            $("#impPrice").val(price);
            $("select").val(subid);
            //alert(id);
            $("#impId").val(id);
            $("#addImprovement").val("EDIT");
            $("form h2").text("Edit improvement");
            $("form").attr("action","improvements/editImprovement");
            //alert($("form").attr("action"));
        });
    });
    
    $(".setMark").click(function(e){
        e.preventDefault();
        var id = $(this).parent().parent().attr("class").split(' ')[1];
        var sid = $(this).parent().children(".sid").val();
        var mark = $(this).parent().children(".hwMark").val();
        var showMark = $(this).parent().parent().children(".taskMark");
        //alert("id: "+id+", sid: "+sid+", mark: "+mark);
        $.ajax({
            type: 'POST',
            data: {homework_id: id, student_id: sid, mark: mark},
            url: '../../subjects/setStudentMark',
            success: function(r){
                showMark.text("Mark: "+mark);
            }
        });
    });
    
    $(".editSubject,.editSection").click(function(e){
        e.preventDefault();
        
        var text = $(this).parent().parent().children(".section_name").text();
        //alert($(this).attr("class"));
        if($(this).attr("class") === "editSubject"){
            var id = $(this).parent().parent().attr("id");
            var sid = 0;
            var type = "editSubject";
        } else{
            var id = $(this).parent().parent().attr("id");
            var sid = $(this).parent().parent().parent().attr("id");
            var type = "editSection";
        }
        //var title = ;
        if($('body').scrollTop() === 0){
            changeAddToEdit(id,text,type,sid);
        } else{
            $('body').animate({ scrollTop: 0 }, 'slow', function(){
                changeAddToEdit(id,text,type,sid);
            });
        }
    });
    
    
    /*$(".editSection").click(function(e){
        e.preventDefault();
        var id = $(this).parent().parent().attr("id");
        var text = $(this).parent().parent().children(".section_name").text();
        //var title = ;
        if($('body').scrollTop() === 0){
            $("#subText").val(text);
            $("form h2").text("Edit subject");
            $("form #addImprovement").val("Edit");
            $("form").attr("action","subjects/editSubject");
        } else{
            $('body').animate({ scrollTop: 0 }, 'slow', function(){
                $("#subText").val(text);
                $("form h2").text("Edit subject");
                $("form #addImprovement").val("Edit");
                $("form").attr("action","subjects/editSubject");
            });
        }
    });*/
    
    $("#nav-log").click(function() {
        logClick();
    });
    $("#nav-reg").click(function() {
        regClick();
    });
    $(".subinfo").mouseover(function() {
        //$(this).css('background-image', 'url("public/images/subjectS.png")');
        $(this).css('color', '#FF5E5E');
    });
    $(".subinfo").mouseout(function() {
        //$(this).css('background-image', 'url("public/images/subject.png")');
        $(this).css('color', '#FF5E5E');
    });
    /*$("#userType").click(function() {
     $("#class").hide();
     });*/
    $("#userType").change(function() {
        if ($("#userType").val() == "1") {
            $('[name=class]').val( '' );
            $("#classType").hide("slow");
            
            //$(".li6").hide("slow");
        } else {
            $("#classType").show("slow");
            //$(".li6").show("slow");
            //$('.mRequired').removeClass('required');
        }
    });
    
    $("#subjectSelect").change(function() {
        if ($("#subjectSelect").val() != "0") {
            $("#sectionSelect").show("slow");
            var id = $("#subjectSelect").val();
            $("#sectionSelect").load("subjects/getSections/"+id);
            $("#classStudents").load("subjects/changeStudents/"+id);
        } else{
            $("#sectionSelect").hide("slow");
        }
    });
    
    $("#sectionSelect").change(function() {
        if ($("#sectionSelect").val() != "0") {
            $("#homeworkSelect").show("slow");
            var id = $("#sectionSelect select").val();
            $("#homeworkSelect").load("subjects/getHomeworks/"+id);
        } else{
            $("#homeworkSelect").hide("slow");
        }
    });
    
    $("#userType").change(function(){
        var id = $("#userType").val();
        $("#classStudents").load("achievements/changeClass/"+id);
        //window.location.replace("http://followtasks.com/achievements/changeClass/"+id);
    });
    
    $(".allStudents").change(function(){
        if($('.allStudents').attr('checked')==="checked"){
            $('#classStudents').hide("slow");
        } else{
            $('#classStudents').show("slow");
            $("#classStudents").load("achievements/changeClass/"+id);
        }
    });
    
    $("#logform").on("submit",function (event){
        //alert("submit");
        $(this).submit();
        //alert(window.isFormValid);
        //if (window.isFormValid != true) {
        //$("#logform").unbind("submit");
        //alert(document.getElementById('logform').text());
        //document.getElementById('logform').submit();
        //validateLogin();
        event.preventDefault();
        return false;
        //}
            
        
    });
    //$('textarea').autoResize();
    validateFormKeyup();
    //validateMail();
    $(".clickable_section").click(function (){
        var item = $(this).parent(".section").children(".homeworks");
        if(item.css("display")==="none"){
            $(".homeworks").hide("slow");
            item.show("slow");
        }
        else{
            item.hide("slow");
        }
        
    });
    
    if(p === "http://www.followtasks.com/subjects"){
        window.location.replace(window.location.pathname + '#actual');
    }
    if(path === "#all"){
        //alert("all");
        showHw($("#all").attr("id"));
    } else if(path === "#actual"){
        showHw($("#actual").attr("id"));
    }
    
    $(".showHw").click(function() {
        var id = this.id;
        //alert(id);
        showHw(id);
    });
    
    
    $(".sort_by").click(function() {
        //alert("neco");
        var id = this.id; 
        $(".section div").animate({
            opacity: 0,
            height: "0%"
        }, 700, function() {
            //$( "#content" ).load("ranking/sort_by/"+this.id);
            $("#rank-table").load("ranking/sort_by/"+id);
            $(".section div").hide();
        });
        //$(".section div").show();
        //alert("ranking/sort_by/"+id);
        $(".section div").animate({
                opacity: 100,
                height: "3%"
            }, 700, function() {
                
        });
        
    });
    
    $('#changeLanguage').click(function(){
        if($('#languages').css('display') === "none"){
            $('#languages').show();
        }
        else{
            $('#languages').hide();
        }
    });
    
    $(".language").click(function(){
        var data = $(this).attr("id");
        //alert(data);
        $.ajax({
            type: 'POST',
            url: 'index/language/'+data,
            success: function(){
                location.reload();
            }
        });
    });
    
    $(".numberImput").keydown(function(e){
        //alert(e.keyCode);
        //if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        if(e.keyCode > 64 && e.keyCode < 91){
            //$("#impPrice").text("a");
            e.preventDefault();
        }
    });
});

function showHw(id) {
    if (id === "all") {
        $("#actualHw").hide("slow");
        $("#allHw").show();
        window.location.replace(window.location.pathname + '#all');
    } else if (id === "actual") {
        $("#allHw").hide("slow");
        $("#actualHw").show("slow");
        window.location.replace(window.location.pathname + '#actual');
    }
}

function changeAddToEdit(id, text, type, sid){
    $("#subText").val(text);
    $("form h2").text("Edit subject");
    $("form #addImprovement").val("Edit"); 
    //alert("subjects/"+type+"/"+id);
    var path;
    if(sid === 0){
        path = "../../subjects/"+type+"/"+id;
    } else{
        path = "../../subjects/"+type+"/"+id+"/"+sid;
    }
    $("form").attr("action",path);
}
    
function sortByHomeworks(){
    $( "#content" ).load( "ranking/sortByHomeworks" );
}
function sortByLevels(){
    $( "#content" ).load( "ranking/sortByLevels" );
}

function logClick() {
    $("#nav-reg").removeClass("nav-active");
    $("#nav-log").addClass("nav-active");
    $(".insidelogin").delay(400).fadeIn(400);
    $(".insidereg").fadeOut(400);
    $("#container").animate({height: "450px"});
    window.location.replace(window.location.pathname + '#log');
}
function regClick() {
    $("#nav-log").removeClass("nav-active");
    $("#nav-reg").addClass("nav-active");
    $(".insidereg").delay(400).fadeIn(400);
    $(".insidelogin").fadeOut(400);
    
    if(window.innerHeight > window.innerWidth){
        $("#container").animate({height: "50%"});
    } else{
        $("#container").animate({height: "92%"});
    }
    window.location.replace(window.location.pathname + '#reg');
}
function forgottenClick(){
    
}

function validateMail() {
    //$("#new_email").focusout(function() {

    //var data = "email=" + $("#email").val();
    var data = $("#new_email").val();
    var url = 'index/ajaxReq/' + data;
    //alert(url);
    //alert(data);
    $.ajax({
        type: 'POST',
        data: {email: data},
        url: 'index/ajaxReq',
        success: function(result)
        {
            //alert(result);
            if (result === '1'){
                //$("#mail_error").text("Email already exists");
                $("#email_error").show();
                return false;
            } else {
                //$("#mail_error").text("Email can be used");
                $("#email_error").hide();
                return true;
            }
        }
    });

    //});
}
function validateLogin() {
    var email = $("#logmail").val();
    var pass = $("#logpass").val();
    if (email.length == 0 || pass.length == 0) {
        $("#ajax_message").text("Fill the information before login.");
    }
    else {
        var data = {email: email, pass: pass};
        var form = document.getElementById("logform");
        //alert(data.email + ', ' + data.pass);
        $.ajax({
            type: 'POST',
            data: { email: email, pass: pass },
            url: 'index/ajaxLogin/' + data.email + '/' + data.pass,
            //url: 'index/ajaxLogin',
            dataType: 'json',
            success: function(result)
            {
                //alert(result);
                if (result == "1")
                {
                    $("#ajax_message").text("Email or password is not correct.");
                    //window.isFormValid = false;
                } else {
                    $("#ajax_message").text("Ok.");
                    //alert("ok");
                    //alert($("#logform").text());
                    //window.isFormValid = true;
                    //$("#logform").off('submit');
                    //document.getElementById('logform').submit();
                    
                    //$("#logform").unbind("submit");
                    //$("#logform").submit();
                    //$("#logform").submit();
                }
            }

        });
    }
}
function validateForm() {
    //var error_message = "";
    var is_error = false;
    if ($("#name").val() == "") {
        is_error = true;
        $('#name_error').show();
    }
    if ($("#surname").val() == "") {
        is_error = true;
        $('#surname_error').show();
    }
    if ($("#pass").val().length < 5) {
        is_error = true;
        $('#password_error').show();
    }
    if ($("#pass").val() == "" || $("#pass_again").val() == "") {
        //error_message = error_message + "You need to insert password. ";
    }
    if ($("#pass").val() != "" && $("#pass").val() != $("#pass_again").val()) {
        is_error = true;
        $('#password_again_error').show();
    }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailVal = $("#new_email").val();
    if (!emailReg.test(emailVal) || emailVal == "") {
        is_error = true;
        $('#email_valid_error').show();
    }
    /*else {
        //is_error = validateMail();
    }*/
    if (!is_error) {
        document.getElementById("regform").submit();
    }
}
function validateFormKeyup() {
    $("#name").bind("propertychange keyup input paste change", function() {
        if ($("#name").val() != "") {
            $("#name_error").hide();
        }
    });
    $("#surname").bind("propertychange keyup input paste change", function() {
        if ($("#surname").val() != "") {
            $("#surname_error").hide();
        }
    });
    $("#pass_again").keyup(function() {
        //alert("Pass again");
        if ($("#pass").val() != $("#pass_again").val()) {
            //error_message = error_message + "Passwords are different. ";
            //$("#pass_again_error").text("Passwords are different. ");
            $('#password_again_error').show();
        }
        else {
            $("#pass_again_error").text("");
            $('#password_again_error').hide();
        }
        if ($("#pass").val() == "") {
            $('#password_again_error').hide();
        }
    });
    $("#pass").keyup(function() {
        if ($("#pass").val() != "" && $("#pass").val().length < 5) {
            //$('#pass_error').text("Password is too short. ");
            $('#password_error').show();
            //error_message = error_message + "Password is too short. ";
        }
        else {
            $("#pass_error").text("");
            $('#password_error').hide();
            $('#password_again_error').hide();
        }
    });
    $("#new_email").bind("propertychange keyup input paste change", function() {
        //alert(emailReg.test(emailaddressVal));
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var emailVal = $("#new_email").val();
        if (!emailReg.test(emailVal)) {
            //$("#mail_error").text("Email is not valid. ");
            $("#email_error").hide();
            $("#email_valid_error").show();
        }
        else {
            $("#mail_error").text("");
            $("#email_valid_error").hide();
            validateMail();
        }
    });

    $(".addImp").click(function(){
        if($(this).attr("value") !== "Bought") {
            //var price = $(".addImp").val();
            var price = $(this).attr("class","addImp").val();
            $(this).attr("value", "Bought");
            $(this).addClass("bought");
            //alert(price);
            $.ajax({
                type: 'POST',
                data: {id: this.id, price: price},
                url: 'improvements/buyImprovement'
            });
        }
    });
    
    $(".deleteSection").click(function(e){
       e.preventDefault();
       //alert("ok");
       var parent = $(this).parent().parent();
       var id = parent.attr("id");
       
       $.ajax({
            type: 'POST',
            data: {id: id},
            url: '../../subjects/deleteSection',
            success: function(){
                parent.hide("slow");
            }
        });
    });
}